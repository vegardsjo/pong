#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <malloc.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <dirent.h>
#include "graphics/framebuffer.h"
#include "graphics/image.h"
#include "graphics/graphics.h"
#include "object.h"

#define LINE_MAX (320 / 8)
#define RESOURCES "resources_x86/"

image_t* background;
anim_t* charmap;
object_t* obj;

void draw_text_vararg(int x, int y, const char* format, va_list ap) {
  static char buffer[LINE_MAX];
  vsnprintf(buffer, LINE_MAX, format, ap);
  blit_text(charmap, buffer, x, y);
}

void draw_text(int x, int y, const char* format, ...) {
  va_list ap;
  va_start(ap, format);
  draw_text_vararg(x, y, format, ap);
  va_end(ap);
}

int main(void) {
  srand(time(NULL));
  
  init_graphics();

  background = read_image("workdir/Lines.ppm");
    
  image_t* red = read_image("workdir/Lines_Red.ppm");
  image_t* blue = read_image("workdir/Lines_Blue.ppm");
  image_t* green = read_image("workdir/Lines_Green.ppm");
  image_t* LINES[] = {red, blue, green};
  image_t* lines;

  charmap = read_animation(RESOURCES "charmap.ani");
  
  obj = object_new("workdir/fish.ppm", FB_WIDTH/2, FB_HEIGHT);
  obj->speed.deg = M_PI/2;
  obj->speed.val = 20;


  blit_full(background);
  uint8_t i = 0;
  while(true) {
    lines = LINES[i++ % 3];
    //blit_fast_clip(red, (rect_t){0, 0, 50, 50}, 0, 0);
    //blit_fast_clip(red, (rect_t){0, 0, 50, 50}, 0, 100);
    object_draw_below(lines, obj);
    object_update_pos(obj);
    object_draw(obj);

    if (i > 128)
     obj->speed.deg = acos(-sin(obj->speed.deg));

    usleep(2500);
    sb_swap();
  }

  return 0;
}
