#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "object.h"
#include "pong.h"

static Pong_Game Pong;

int main(int argc, char* argv[]) {
  bool graphics_ok = init_graphics();
  if (!graphics_ok) {
    fprintf(stderr, "Could not init graphics.\n");
    finish_graphics();
    exit(EXIT_FAILURE);
  }

  // Last bakgrunn fra fil
  image_t background; 
  read_image(&background, "bg.ppm");

  // Lag fiskeobject
  object_t* fish = object_new("fish.ani", 100, 100);

  if (!fish) {
    fprintf(stderr, "Could not load the fish. Try again later.\n");
    free_image(&background);
    exit(1);
  }

  anim_t explode;
  read_anim(&explode, "fish_anim_explode.ani");

  // Enda en fisk, men dette er en ond fisk som går mot strømmen.
  object_t* evilfish = object_new("evilfish.ani", 700, 100);

  // Og hans enda ondere bror:
  object_t* evilfish_twinbrother = object_new("evilfish_twinbrother.ani", 0, 0);

  fish->speed = (vec_t){0 * (M_PI/180), 50}; // 45 deg, 500 pixels per second
  evilfish->speed = (vec_t){180 * (M_PI/180), 50};
  //evilfish_twinbrother->speed = (vec_t){100 * (M_PI/180), -4};

  bool has_collided = false;
  //for (double v = 0;!does_collide(fish, evilfish) && !does_collide(evilfish, evilfish_twinbrother) && !does_collide(evilfish_twinbrother, fish) ;v += 0.01) {
  for(double v = 0;; v++) {
    blit_full(&background); // Ueffektivt, men dette er testkode
    
    if (does_collide(fish, evilfish) && !has_collided) {
      fish->anim = &explode;
      fish->speed.val *= -1;
      fish->frame = 0;
      evilfish->anim = &explode;
      evilfish->speed.val *= -1;
      evilfish->frame = 0;

      fish->draw(fish);
      evilfish->draw(evilfish);
      has_collided = true;
    }

    if (!has_collided) {
      fish->draw(fish);
      evilfish->draw(evilfish);
    }
    else if (fish->frame != fish->anim->frames-1) {
      fish->draw(fish);
      evilfish->draw(evilfish);
    }

    //evilfish->speed.deg = M_PI * sin(rand() * 1000);
    //evilfish_twinbrother->draw(evilfish_twinbrother);
    //evilfish_twinbrother->speed.val = 300 * sin(v);
    //evilfish_twinbrother->speed.deg = M_PI * sin(v);
    
    /* collision check */
    /*
    if (does_collide(fish, evilfish)) {
      fprintf(stderr, "Fish collides with evil fish.\n");
      exit(EXIT_FAILURE);
    }
    */

    sb_swap();
  }

  return 0;
}
