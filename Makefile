########################################
#                                      #
# TDT4258 Mikrokontroller systemdesign #
# Øving 3                              # 
#                                      #
# sjonfjel                             #
# tirilane                             #
#                                      #
# Makefile                             #
#                                      #
########################################

# program
PROGRAM = pong.elf

# objektfiler
OBJS = pong.o object.o sound/sound.o graphics/graphics.o graphics/image.o driver/button_interface.o driver/led_interface.o utils/endian.o

# kildefiler
SOURCES = pong.c object.c sound/sound.c graphics/graphics.c graphics/image.c driver/button_interface.c driver/led_interface.c utils/endian.c


# debug
DEBUG = # -g
STRIP = -s

# kompilatoropsjoner
CFLAGS = $(DEBUG) -std=gnu99 -O3 -ffast-math -fomit-frame-pointer -Wall -Wextra -pedantic

# linker
LD = avr32-linux-gcc

# kompilator
CC = $(LD)

# linkeropsjoner
LDFLAGS = -std=gnu99 -ldl -lpthread -lm -lc -static $(STRIP)

# c/h -> o
$(PROGRAM): $(OBJS) $(SOURCES)
	$(LD) $(OBJS) $(LDFLAGS) -o $(PROGRAM)

pong.o: pong.c pong.h object.h
	$(CC) $(CFLAGS) -c $<

object.o: object.c object.h 
	$(CC) $(CFLAGS) -c $<

sound/sound.o: sound/sound.c sound/sound.h
	$(CC) $(CFLAGS) -c $< -o sound/sound.o

graphics/graphics.o: graphics/graphics.c graphics/graphics.h graphics/image.h graphics/framebuffer.h
	$(CC) $(CFLAGS) -c $< -o graphics/graphics.o

graphics/image.o: graphics/image.c graphics/image.h graphics/framebuffer.h
	$(CC) $(CFLAGS) -c $< -o graphics/image.o

driver/button_interface.o: driver/button_interface.c driver/button_interface.h
	$(CC) $(CFLAGS) -c $< -o driver/button_interface.o

driver/led_interface.o: driver/led_interface.c driver/led_interface.h
	$(CC) $(CFLAGS) -c $< -o driver/led_interface.o

utils/endian.o: utils/endian.c utils/endian.h
	$(CC) $(CFLAGS) -c $< -o utils/endian.o

# fjern alle autogenererte filer
.PHONY : clean
clean : 
	rm -rf $(OBJS) $(PROGRAM)
