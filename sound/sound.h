#ifndef SOUND_H
#define SOUND_H

#include <stdint.h>
#include <stdbool.h>

#define DSP_DEVICE "/dev/dsp"
#define WAVE_CHANNELS_OFFSET 22
#define WAVE_SAMPLES_OFFSET 24
#define WAVE_BPS_OFFSET 34
#define WAVE_DATA_OFFSET 44
#define BYTES_AT_ONCE 1024

typedef struct {
  uint16_t samplerate;
  uint16_t bitrate;
  uint16_t channels;
  size_t size;
  uint8_t* pcm_data;
} sound_t;

bool init_sound(void);
void finish_sound(void);
sound_t* load_sound(const char*);
void stop_sound(void);
void play_sound(sound_t*);
void free_sound(sound_t*);

#endif
