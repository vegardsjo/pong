#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/soundcard.h>
#include "utils/endian.h"
#include "sound.h"

static int dsp_file = 0;
static volatile bool playing;

bool init_sound(void) {
  dsp_file = open(DSP_DEVICE, O_WRONLY);
  if (dsp_file < 0) {
    fprintf(stderr, "Could not open %s for writing: %s\n", DSP_DEVICE, strerror(errno));
    return false;
  }

  return true;
}

void finish_sound(void) {
  ioctl(dsp_file, SNDCTL_DSP_RESET, 0);
  ioctl(dsp_file, SNDCTL_DSP_SYNC, 0);
  fsync(dsp_file);
  close(dsp_file);
}

sound_t* load_sound(const char* filename) {
  int sound_file = open(filename, O_RDONLY);
  if (sound_file < 0) {
    fprintf(stderr, "Could not open %s for reading: %s\n", filename, strerror(errno));
    return NULL;
  }

  struct stat stat_buf;
  fstat(sound_file, &stat_buf);

  if (stat_buf.st_size < 100) {
    fprintf(stderr, "%s can not be a wave file.\n", filename);
    close(sound_file);
    return NULL;
  }

  sound_t* sound = calloc(1, sizeof(sound_t));
  sound->size = stat_buf.st_size - WAVE_DATA_OFFSET;

  sound->pcm_data = calloc(1, sound->size);
  if (sound->pcm_data == NULL) {
    fprintf(stderr, "Could not allocate sound data for file %s.\n", filename);
    free(sound);
    close(sound_file);
    return NULL;
  }

  lseek(sound_file, WAVE_CHANNELS_OFFSET, SEEK_SET);
  read(sound_file, &sound->channels, sizeof(sound->channels));
  sound->channels = le16toho(sound->channels);
  
  lseek(sound_file, WAVE_SAMPLES_OFFSET, SEEK_SET);
  read(sound_file, &sound->samplerate, sizeof(sound->samplerate));
  sound->samplerate = le16toho(sound->samplerate);

  lseek(sound_file, WAVE_BPS_OFFSET, SEEK_SET);
  read(sound_file, &sound->bitrate, sizeof(sound->bitrate));
  sound->bitrate = le16toho(sound->bitrate);
  
  lseek(sound_file, WAVE_DATA_OFFSET, SEEK_SET);
  read(sound_file, sound->pcm_data, sound->size);

  //printf("Loaded wave file '%s': channels: %hu, samplerate: %hu, bitrate: %hu\n", filename, sound->channels, sound->samplerate, sound->bitrate);

  return sound;
}

void free_sound(sound_t* sound) {
  if (sound == NULL)
    return;

  free(sound->pcm_data);
  free(sound);
}

void stop_sound(void) {
  // Reset sound device; cancel playing of last sound
  ioctl(dsp_file, SNDCTL_DSP_RESET, 0);
}

void play_sound(sound_t* sound) {
  int samplerate = sound->samplerate, bitrate = sound->bitrate, channels = sound->channels;
  //printf("writing %d bytes to dsp, samples: %hu, bits: %hu, channels: %hu\n", sound->size, samplerate, bitrate, channels);
  
  // Set the sample rate, bitrate and number of channels 
  ioctl(dsp_file, SOUND_PCM_WRITE_RATE, &samplerate);
  ioctl(dsp_file, SOUND_PCM_WRITE_BITS, &bitrate);
  ioctl(dsp_file, SOUND_PCM_WRITE_CHANNELS, &channels);

  playing = true;

  // Write pcm data to device
  write(dsp_file, sound->pcm_data, sound->size);

  // Assure the data has been written
  fsync(dsp_file);
  ioctl(dsp_file, SNDCTL_DSP_SYNC, 0);

  playing = false;
}
