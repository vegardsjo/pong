#include <SDL2/SDL_audio.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/soundcard.h>
#include "utils/endian.h"
#include "sound.h"
#include <SDL2/SDL.h>

struct UserData {
  SDL_AudioSpec spec;
  size_t current;
  uint8_t **buf;
  uint32_t *pos;
  uint32_t *len;
  bool device_opened;
};

static struct UserData data;

bool init_sound(void) {
  data.current = 0;
  data.buf = malloc(sizeof(uint8_t*) * 10);
  data.pos = malloc(sizeof(size_t) * 10);
  data.len = malloc(sizeof(size_t) * 10);
  return true;
}

void finish_sound(void) {
}

static void audio_callback(void *userdata, uint8_t *stream, int len) {
  (void)userdata;
  len = len > (data.len[data.current] - data.pos[data.current]) ? (data.len[data.current] - data.pos[data.current]) : len;

  if (len <= 0) {
    SDL_PauseAudio(1);
    return;
  }

  printf("Callback stream: current: %lu, %p, pos: %lu, len: %d \n", data.current, stream, data.pos[data.current], len);
  SDL_memcpy(stream, data.buf[data.current] + data.pos[data.current], len);

  data.pos[data.current] += len;
}

sound_t* load_sound(const char* filename) {
  printf("Load sound: %s\n", filename);
  data.device_opened = false;

  if (!SDL_LoadWAV(filename, &data.spec, &data.buf[data.current], &data.len[data.current])) {
    fprintf(stderr, "Could not load wav file %s: %s\n", filename, SDL_GetError());
    return NULL;
  }

  data.spec.callback = audio_callback;
  data.spec.userdata = NULL;

  sound_t* sound = calloc(1, sizeof(sound_t));
  sound->size = data.current;
  data.current++;
  return sound;
}

void free_sound(sound_t* sound) {
  if (sound == NULL)
    return;

  size_t current = sound->size;

  SDL_FreeWAV(data.buf[current]);
  free(sound);
}

void stop_sound(void) {
  printf("Stop sound\n");
  /* SDL_PauseAudio(1); */
  /* SDL_CloseAudio(); */
}

void play_sound(sound_t* sound) {
  printf("Play sound\n");
  size_t current = sound->size;
  data.current = current;

  if (!data.device_opened) {
    if (SDL_OpenAudio(&data.spec, NULL) != 0) {
      fprintf(stderr, "Could not open audio device: %s\n", SDL_GetError());
    }
    data.device_opened = true;
  }

  data.pos[data.current] = 0;
  SDL_PauseAudio(0);
}
