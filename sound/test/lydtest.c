#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <linux/soundcard.h>

int main(int argc, char* argv[]) {
  if (argc < 4) {
    fprintf(stderr, "Usage: lydtest <file.wav> <samplerate> <bits> <volume>\n");
    exit(1);
  }

  FILE* fp_in = fopen(argv[1], "rb");
  if (fp_in == NULL) {
    fprintf(stderr, "Could not open '%s' for reading: %s\n", argv[1], strerror(errno));
    exit(1);
  }

  long samplerate = strtol(argv[2], NULL, 10);
  long bits = strtol(argv[3], NULL, 10);
  long volume = strtol(argv[4], NULL, 10);
  long channels = 1;
  
  FILE* fp_dsp = fopen("/dev/dsp", "wb");
  if (fp_dsp == NULL) {
    fprintf(stderr, "Could not open /dev/dsp for writing: %s\n", strerror(errno));
    exit(1);
  }
  
  ioctl(fileno(fp_dsp), SOUND_MIXER_VOLUME, &volume);
  ioctl(fileno(fp_dsp), SOUND_MIXER_PCM, &volume);
  ioctl(fileno(fp_dsp), SOUND_MIXER_SPEAKER, &volume);
  ioctl(fileno(fp_dsp), SOUND_MIXER_LINE, &volume);
  ioctl(fileno(fp_dsp), SOUND_MIXER_LINE1, &volume);
  ioctl(fileno(fp_dsp), SOUND_MIXER_LINE2, &volume);
  ioctl(fileno(fp_dsp), SOUND_MIXER_LINE3, &volume);

  ioctl(fileno(fp_dsp), SOUND_PCM_WRITE_RATE, &samplerate);
  ioctl(fileno(fp_dsp), SOUND_PCM_WRITE_BITS, &bits);
  ioctl(fileno(fp_dsp), SOUND_PCM_WRITE_CHANNELS, &channels);
 
  unsigned char buf[4096];
    
  fseek(fp_in, 44, SEEK_CUR); // skip to data part of wave file, see http://ccrma.stanford.edu/courses/422/projects/WaveFormat
 
  // write to dsp until either eof or error
  while (!feof(fp_in) && !ferror(fp_in) && !ferror(fp_dsp)) {
    size_t bytes_read = fread(buf, 1, sizeof(buf)/sizeof(buf[0]), fp_in); // liten hack: leser 4096 elementer av størrelse 1 for å få tilbake antall leste bytes
    fwrite(buf, bytes_read, 1, fp_dsp);
  }

  if (ferror(fp_in)) printf("Error reading '%s'.\n", argv[1]);
  if (ferror(fp_dsp)) printf("Error writing to DSP.\n");
}

