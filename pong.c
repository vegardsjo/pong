#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <malloc.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>
#include <sys/time.h>
#include <dirent.h>
#include "driver/button_interface.h"
#include "driver/led_interface.h"
#include "graphics/framebuffer.h"
#include "graphics/image.h"
#include "graphics/graphics.h"
#include "sound/sound.h"
#include "object.h"
#include "pong.h"
#include <SDL2/SDL.h>
#include <assert.h>

volatile static Pong_t Pong;
volatile static Sound_t Sound;
volatile static Threads_t Threads;

// Moves a paddle
void move_paddle(object_t* paddle, direction_t direction) {
  paddle->speed.deg = asin(direction);
  paddle->speed.val = PADDLE_MOVE_DEFAULT;
}

void signal_handler(int unused) {
  (void)unused;

  // Try to aquire the exit lock. If not, just exit from the thread
  if (pthread_mutex_trylock(&Threads.exit_mutex) != 0)
    exit(EXIT_SUCCESS);

  // Mutex lock aquired, free the resources
  fprintf(stderr, "Signal SIGINT caught! Exiting ...\n");
  finish_app();
  exit(EXIT_SUCCESS);
}

void* device_thread_func(void* unused) {
  (void)unused;
  button_setting_t buttons;

  int buttons_fd = open_buttons();
  if (buttons_fd < 0) {
    pthread_exit(NULL);
    return NULL;
  }

  int leds_fd = open_leds();
  
  bool ai_left_down = false;
  bool ai_right_down = false;
  bool reset_down = false;

  while (Pong.program_running) {
    usleep(200);

    buttons = read_buttons(buttons_fd);
    if (buttons.sw4) {
      exit(EXIT_SUCCESS);
    }

    if (buttons.sw3) reset_down = true;

    // Reset spill ved sw3-slipp
    if (!buttons.sw3 && reset_down) {
      reset_game();
      reset_down = false;
      continue;
    }
    if (!Pong.game_running) {
      usleep(1000);
      continue;
    }

    if (buttons.sw0) move_paddle(Pong.paddle_right, DIR_DOWN);
    if (buttons.sw1) ai_right_down = true;
    if (buttons.sw2) move_paddle(Pong.paddle_right, DIR_UP);
    if (buttons.sw5) move_paddle(Pong.paddle_left, DIR_DOWN);
    if (buttons.sw6) ai_left_down = true;
    if (buttons.sw7) move_paddle(Pong.paddle_left, DIR_UP);

    // Stopp paddles ved knappeslipp
    if (!buttons.sw0 && !buttons.sw2)
      object_stop(Pong.paddle_right);

    if (!buttons.sw5 && !buttons.sw7)
      object_stop(Pong.paddle_left);

    // Toggle venstre AI ved sw6-slipp
    if (!buttons.sw6 && ai_left_down) {
      Pong.ai_left = !Pong.ai_left;
      ai_left_down = false;
    }

    // Toggle høyre AI ved sw1-slipp
    if (!buttons.sw1 && ai_right_down) {
      Pong.ai_right = !Pong.ai_right;
      ai_right_down = false;
    }

    write_leds(leds_fd, *(led_setting_t*)&buttons);

    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      if (event.key.keysym.sym == SDLK_q) {
        Pong.program_running = false;
      }
    }
  }

  close(buttons_fd);
  close(leds_fd);
  pthread_exit(NULL);
  return NULL;
}

// Artificially controlled paddles
void do_ai_paddles(void) {
  static int move_speed = PADDLE_MOVE_DEFAULT;  // max movement speed
  static int rand_off = 0;                      // random offset from center of paddle
  static paddlenum_t paddle = PADDLE_LEFT;      // the current moving paddle

  if (Pong.ai_left && Pong.ball->speed.deg > M_PI/2.0 && Pong.ball->speed.deg < M_PI*3.0/2.0) {
    if (paddle == PADDLE_RIGHT) {
      paddle = PADDLE_LEFT;
      rand_off = sin(rand()) * (Pong.paddle_left->height/2 - 4);
    }

    if (Pong.paddle_left->y+rand_off > Pong.ball->y) {
      object_move(Pong.paddle_left, Pong.paddle_left->x, Pong.paddle_left->y - min(Pong.paddle_left->y+rand_off - Pong.ball->y, move_speed));
    }
    else if (Pong.paddle_left->y+rand_off < Pong.ball->y) {
      object_move(Pong.paddle_left, Pong.paddle_left->x, Pong.paddle_left->y + min(Pong.ball->y - Pong.paddle_left->y+rand_off, move_speed));
    }
  } 
  else if (Pong.ai_right && (Pong.ball->speed.deg < M_PI/2.0 || Pong.ball->speed.deg > M_PI*3.0/2.0)) {
    if (paddle == PADDLE_LEFT) {
      paddle = PADDLE_RIGHT;
      rand_off = sin(rand()) * (Pong.paddle_right->height/2 - 4);
    }

    if (Pong.paddle_right->y+rand_off > Pong.ball->y) {
      object_move(Pong.paddle_right, Pong.paddle_right->x, Pong.paddle_right->y - min(Pong.paddle_right->y+rand_off - Pong.ball->y, move_speed));
    }
    else if (Pong.paddle_right->y+rand_off < Pong.ball->y) {
      object_move(Pong.paddle_right, Pong.paddle_right->x, Pong.paddle_right->y + min(Pong.ball->y - Pong.paddle_right->y+rand_off, move_speed));
    }
  }
}

void check_collision(void) {
  // Get next ball displacement
  tuple_t pos_diff = object_calc_next_disp(Pong.ball);
  int new_x = Pong.ball->x + pos_diff.x;
  int new_y = Pong.ball->y + pos_diff.y;

  // Left paddle
  if (object_collides(Pong.ball, Pong.paddle_left, pos_diff.x, pos_diff.y)) {
    Pong.ball->speed.deg = acos(-cos(Pong.ball->speed.deg)) + asin(-((double)min(max(Pong.ball->y - Pong.paddle_left->y, 1), -1) / Pong.paddle_left->height));
    Pong.ball->speed.val *= BALL_SPEED_INC_FACTOR;
    set_sound(SOUND_HIT);
  }
  else
  // Right paddle
  if (object_collides(Pong.ball, Pong.paddle_right, pos_diff.x, pos_diff.y)) {
    Pong.ball->speed.deg = acos(-cos(Pong.ball->speed.deg)) - asin(-((double)min(max(Pong.ball->y - Pong.paddle_right->y, 1), -1) / Pong.paddle_right->height));
    Pong.ball->speed.val *= BALL_SPEED_INC_FACTOR;
    set_sound(SOUND_HIT);
  }
  else
  // Left wall
  if (Pong.ball->box.x + pos_diff.x < 0) {
    Pong.player_two_score++;
    reposition_ball();
    set_sound(SOUND_DIE);
  }
  // Right wall
  else
  if (Pong.ball->box.x + Pong.ball->box.width + pos_diff.x > FB_WIDTH) {
    Pong.player_one_score++;
    reposition_ball();
    set_sound(SOUND_DIE);
  }
  // Top and bottom wall
  else
  if ((Pong.ball->box.y + pos_diff.y <= 0) || (Pong.ball->box.y + Pong.ball->box.height + pos_diff.y > FB_HEIGHT)) {
    Pong.ball->speed.deg = 2*M_PI - Pong.ball->speed.deg;
    set_sound(SOUND_WALL);
  }
  
  // Keep ball at maximum speed
  if (Pong.ball->speed.val > BALL_SPEED_MAX)
    Pong.ball->speed.val = BALL_SPEED_MAX;
}

// Set a random background image from the wallpaper list
void set_random_background(void) {
  free_image(Pong.background);

  int num = rand() % (int)Pong.num_wallpapers;
  Pong.background = read_image(Pong.wallpapers[num]);
}

// Reset the game, starting a new one
void reset_game(void) {
  Pong.game_running = false;
  Pong.player_one_score = 0;
  Pong.player_two_score = 0;
  Pong.ai_left = false;
  Pong.ai_right = false;

  // Move objects to their starting position
  object_move(Pong.paddle_left, 10, MEDIAN_Y);
  object_stop(Pong.paddle_left); 
  object_move(Pong.paddle_right, FB_WIDTH - 10, MEDIAN_Y);
  object_stop(Pong.paddle_right); 
  reposition_ball();

  // Choose a random background
  set_random_background();
  blit_full(Pong.background);
  sb_swap();

  Pong.game_running = true;
  set_sound(SOUND_COIN);
}

void reposition_ball(void) {
  #define RANDOM_BALL_VEC_CALC ((double)(rand() % 100) / 100.0 * 2*M_PI)
  static double slack = 0.2;
  //object_draw_below(Pong.background, Pong.ball);
  object_move(Pong.ball, MEDIAN_X, MEDIAN_Y);

  // Random starting degree for ball
  do {
    Pong.ball->speed = (vec_t){RANDOM_BALL_VEC_CALC, BALL_SPEED_DEFAULT};
  } while ((has_value_about (Pong.ball->speed.deg, M_PI/2, slack)) ||
      (has_value_about (Pong.ball->speed.deg, 3*M_PI/2, slack)) ||
      (has_value_about (Pong.ball->speed.deg, M_PI, slack)) ||
      (has_value_about (Pong.ball->speed.deg, 2*M_PI, slack)));
}

bool has_value_about (double testing_value, double value, double slack) {
  if ((testing_value > value - slack) && testing_value < value + slack)
    return true;
  return false;
}

// Main graphics loop
void* game_thread_func(void* unused) {
  (void)unused;
  char text_buffer[1024];

  if (!init_pong()) {
    Pong.program_running = false;
    pthread_exit(NULL);
    return NULL;
  }

  while (Pong.program_running)
  {
    // If the game is paused, do not update the objects
    if (!Pong.game_running) {
      usleep(1000);
      continue;
    }

    // Tegn bak tekst
    blit_fast_clip(Pong.background, (rect_t){0, 0, FB_WIDTH, 32} , 0, 0);
    blit_fast_clip(Pong.background, (rect_t){0, FB_HEIGHT-16, FB_WIDTH, 16}, 0, FB_HEIGHT-16);
    
    // Tegn bakgrunn over forrige frame
    //object_draw_below(Pong.background, Pong.paddle_left);
    //object_draw_below(Pong.background, Pong.paddle_right);
    object_draw_below(Pong.background, Pong.ball);
    blit_fast_clip(Pong.background, (rect_t){0, 0, 36, FB_HEIGHT} , 0, 0);
    blit_fast_clip(Pong.background, (rect_t){FB_WIDTH-36, 0, 36, FB_HEIGHT}, FB_WIDTH-36, 0);

    // AI :P
    do_ai_paddles();
    
    // Oppdater posisjon av ballen og tegn
    object_update_pos(Pong.ball);
    object_draw(Pong.ball);
    
    // Tegn paddler
    object_update_pos(Pong.paddle_left);
    object_draw(Pong.paddle_left);

    object_update_pos(Pong.paddle_right);
    object_draw(Pong.paddle_right);

    // Skriv statustekst
    snprintf(text_buffer, sizeof(text_buffer), "P2 Score: %d", Pong.player_two_score);
    draw_text(FB_WIDTH - strlen(text_buffer)*Pong.charmap->f_width - 1, 1, "%s", text_buffer);
    
    snprintf(text_buffer, sizeof(text_buffer), "P2 AI: %s", (Pong.ai_right ? "On" : "Off"));
    draw_text(FB_WIDTH - strlen(text_buffer)*Pong.charmap->f_width - 1, 20, "%s", text_buffer);
    
    // Speed and degree display (debug)
    //snprintf(text_buffer, sizeof(text_buffer), "Speed: %.2f, Deg: %.2f", Pong.ball->speed.val, Pong.ball->speed.deg);
    //draw_text(FB_WIDTH - strlen(text_buffer)*Pong.charmap->f_width - 1, FB_HEIGHT - 15, "%s", text_buffer);

    draw_text(1, 1, "P1 Score: %d", Pong.player_one_score);
    draw_text(1, 20, "P1 AI: %s", (Pong.ai_left ? "On" : "Off"));

    // Skriv ut FPS (debug)
    fps_calc();

    // Sjekk kollisjoner
    check_collision();

    // Begrens frames per sekund
    fps_limit(FRAMES_PER_SECOND);

    // Software buffer swap
    sb_swap();

    // Sjekk score og avslutt om nødvendig
    if (Pong.player_one_score >= WIN_POINTS) {
      announce_image_file(RESOURCES "player_one_wins.img");
      announce_string("SW3 to restart.");
      set_sound(SOUND_WIN);
    } else if (Pong.player_two_score >= WIN_POINTS) {
      announce_image_file(RESOURCES "player_two_wins.img");
      announce_string("SW3 to restart.");
      set_sound(SOUND_WIN);
    }
  }

  pthread_exit(NULL);
  return NULL;
}

void set_sound(soundnum_t num) {
  stop_sound(); // Stop the previous sound
  Sound.current_sound = num;
  Sound.new_sound = true;
}

// This thread waits for a sound event and calls play_sound to play it
void* sound_thread_func(void* unused) {
  (void)unused;
  
  while (Pong.program_running) {
    usleep(600);
    if (Sound.new_sound) {
      Sound.new_sound = false;
      play_sound(Sound.sounds[Sound.current_sound]);
    }
  }

  pthread_exit(NULL);
  return NULL;
}

/* Initialiser spillobjekter */
bool init_pong(void) {
  Pong.player_one_score = 0;
  Pong.player_two_score = 0;

  // Load the sounds
  Sound.sounds = calloc(NUM_SOUNDS, sizeof(sound_t));
  Sound.sounds[SOUND_HIT] = load_sound(RESOURCES "hit.wav");
  Sound.sounds[SOUND_WALL] = load_sound(RESOURCES "wall.wav");
  Sound.sounds[SOUND_DIE] = load_sound(RESOURCES "die.wav");
  Sound.sounds[SOUND_COIN] = load_sound(RESOURCES "coin.wav");
  Sound.sounds[SOUND_WIN] = load_sound(RESOURCES "win.wav");

  /* assert(Sound.sounds[SOUND_HIT] != NULL); */

  // Get the wallpaper list from the wallpaper directory
  Pong.wallpapers = get_wallpaper_list(&Pong.num_wallpapers);
  if (Pong.num_wallpapers == 0 || Pong.wallpapers == NULL) goto pong_init_failed;

  // Set a random background from the list
  set_random_background();
  if (Pong.background == NULL) goto pong_init_failed;

  // Read the character map used for writing text
  Pong.charmap = read_animation(RESOURCES "charmap.ani");
  if (Pong.charmap == NULL) goto pong_init_failed;
  
  Pong.textbox = read_image(RESOURCES "textbox.img");
  if (Pong.textbox == NULL) goto pong_init_failed;
  
  // Initialize the game objects
  Pong.ball = object_new(RESOURCES "ball.img", MEDIAN_X, MEDIAN_Y);
  if (Pong.ball == NULL) goto pong_init_failed;

  Pong.paddle_left = object_new(RESOURCES "paddle_left.ani", 20, MEDIAN_Y);
  if (Pong.paddle_left == NULL) goto pong_init_failed;

  Pong.paddle_right = object_new(RESOURCES "paddle_right.ani", FB_WIDTH - 20, MEDIAN_Y);
  if (Pong.paddle_right == NULL) goto pong_init_failed;

  // Position the ball
  reposition_ball();

  set_sound(SOUND_COIN);
  blit_full(Pong.background); 
  announce_image_file(RESOURCES "logo.img");

  // Success
  return true;

  pong_init_failed:
    fprintf(stderr, "Could not load every object and/or image. Check your installation.\n");
    return false;
}

void finish_pong(void) {
  object_free(Pong.paddle_right);
  object_free(Pong.paddle_left);
  object_free(Pong.ball);
  free_image(Pong.background);
  free_image(Pong.textbox);
  free_animation(Pong.charmap);
  free_wallpaper_list(Pong.wallpapers, Pong.num_wallpapers);

  if (Sound.sounds) {
    for (int i = 0; i < NUM_SOUNDS; i++)
      free_sound(Sound.sounds[i]);
    free(Sound.sounds);
  }

  pthread_mutex_unlock(&Threads.exit_mutex);
  pthread_mutex_destroy(&Threads.exit_mutex);
}

// Initialize the threads
bool init_threads(void) {
  int thread_rc = pthread_create(&Threads.game_thread, NULL, game_thread_func, NULL);
  if (thread_rc) return false;

  thread_rc = pthread_create(&Threads.device_thread, NULL, device_thread_func, NULL);
  if (thread_rc) return false;

  thread_rc = pthread_create(&Threads.sound_thread, NULL, sound_thread_func, NULL);
  if (thread_rc) return false;

  Threads.threads_started = true;
  
  // The exit mutex is used to assure that only one thread deallocates the program's resources.
  pthread_mutex_init(&Threads.exit_mutex, NULL);
  return true;
}

void fps_limit(int fps) {
  static struct timeval last_tv = {0, 0};
  struct timeval cur_tv;
  gettimeofday(&cur_tv, NULL);

  int64_t usec_delta = (cur_tv.tv_sec * 1000000 + cur_tv.tv_usec) - (last_tv.tv_sec * 1000000 + last_tv.tv_usec);
  int64_t fps_delta = (1000000/fps);
  if (usec_delta > 0 && usec_delta <= fps_delta)
    usleep(fps_delta - usec_delta);

  last_tv = cur_tv;
}

void fps_calc(void) {
  static struct timeval last_tv = {0, 0};
  struct timeval cur_tv;
  gettimeofday(&cur_tv, NULL);

  int64_t usec_delta = (cur_tv.tv_sec * 1000000 + cur_tv.tv_usec) - (last_tv.tv_sec * 1000000 + last_tv.tv_usec);
  draw_text(1, FB_HEIGHT - 15, "FPS: %.2llf", 1000000.0L/(long double)usec_delta);

  last_tv = cur_tv;
}

int main(void) {
  bool init_ok;

  init_ok = SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) == 0;
  if (!init_ok) {
    fprintf(stderr, "Could not init SDL: %s.\n", SDL_GetError());
    finish_app();
    exit(EXIT_FAILURE);
  }
  
  // Generer random seed ved applikasjonsstart
  srand(time(NULL));

  // Sett malloc til å ALDRI bruke mmap.
  mallopt(M_MMAP_MAX, 0);

  // Initialiser grafikk
  init_ok = init_graphics();
  if (!init_ok) {
    fprintf(stderr, "Could not init graphics.\n");
    goto init_failed;
  }

  // Initialiser lydsystem
  init_ok = init_sound();
  if (!init_ok) {
    fprintf(stderr, "Could not init sound. Sound disabled.\n");
    goto init_failed;
  }

  Pong.program_running = true;

  // Initialiser tråder
  init_ok = init_threads();
  if (!init_ok) {
    fprintf(stderr, "Could not init threads.\n");
    goto init_failed;
  }

  signal(SIGINT, signal_handler);
  pthread_exit(NULL);

  return 0;

init_failed:
    finish_app();
    pthread_exit(NULL);
    exit(EXIT_FAILURE);
}

void draw_text_vararg(int x, int y, const char* format, va_list ap) {
  static char buffer[LINE_MAX];
  vsnprintf(buffer, LINE_MAX, format, ap);
  blit_text(Pong.charmap, buffer, x, y);
}

void draw_text(int x, int y, const char* format, ...) {
  va_list ap;
  va_start(ap, format);
  draw_text_vararg(x, y, format, ap);
  va_end(ap);
}
 
void announce_string(const char* format, ...) {
  static char buffer[LINE_MAX];
  va_list ap;
  va_start(ap, format);

  Pong.game_running = false;
  
  vsnprintf(buffer, LINE_MAX, format, ap);
  blit_trans(Pong.textbox, 9, 110 + 60);
  draw_text(FB_WIDTH/2 - strlen(buffer)*Pong.charmap->f_width/2, FB_HEIGHT/2 + 60 - Pong.charmap->f_height/2, "%s", buffer);

  sb_swap();
  
  va_end(ap);
}

void announce_image(image_t* image) {
  Pong.game_running = false;

  //blit_full(Pong.background);
  blit_trans(image, FB_WIDTH/2 - image->width/2, FB_HEIGHT/2 - image->height/2);
  
  sb_swap();
}

void announce_image_file(const char* filename) {
  Pong.game_running = false;

  image_t* image = read_image(filename);
  blit_trans(image, FB_WIDTH/2 - image->width/2, FB_HEIGHT/2 - image->height/2);
  free_image(image);

  sb_swap();
}

// Free the allocated resources used by the application
void finish_app(void) {
  Pong.program_running = false;
  finish_sound();
  finish_pong();
  finish_graphics();
}

// Free the wallpaper list
void free_wallpaper_list (char** img_list, size_t length) {
  if (img_list == NULL)
    return;

  for (size_t i = 0; i < length; i++)
    free(img_list[i]);
  
  free(img_list);
}

char **get_wallpaper_list(size_t* length) {
  DIR *dir = opendir (WALLPAPERS); 
  struct dirent *entry;
  if (dir == NULL) {
    *length = 0;
    return NULL;
  }
  
  // Tell antall bilder i katalogen
  size_t num_files = 0;
  while ((entry = readdir (dir)) != NULL)
    if (strstr(entry->d_name, "." EXT_IMG))
      num_files++;

  *length = num_files;
  if (num_files == 0) {
    closedir(dir);
    return NULL;
  }
  
  rewinddir(dir);

  char** files = (char **) calloc (1, num_files * sizeof (*files));
  if (files == NULL) {
    closedir(dir);
    return NULL;
  }
  
  char buf[NAME_MAX];
  size_t cur_file = 0;
  while ((entry = readdir (dir)) != NULL) {
    if (strstr(entry->d_name, "." EXT_IMG)) {
      snprintf(buf, NAME_MAX, "%s/%s", WALLPAPERS, entry->d_name);
      files[cur_file++] = strndup(buf, NAME_MAX);
    }
  }

  closedir (dir);
  return files;
}
