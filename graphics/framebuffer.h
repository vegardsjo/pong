#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include <stdint.h>

#define PIXEL_SIZE 4
#define FB_DEV "/dev/fb0"

#ifdef USE_PC_FB
  #define FB_WIDTH 800
  #define FB_HEIGHT 600
#else
  #define FB_WIDTH 320
  #define FB_HEIGHT 240
#endif

#define FB_PIXELS (FB_WIDTH * FB_HEIGHT)
#define FB_SIZE (FB_PIXELS * PIXEL_SIZE)
#define MEDIAN_X (FB_WIDTH/2)
#define MEDIAN_Y (FB_HEIGHT/2)

typedef uint8_t color_t;

// Pixel data for LCD is BGR with one unused byte
typedef struct {
#if defined(USE_PC_FB)
  color_t b;
  color_t g;
  color_t r;
  color_t a;
#elif defined(USE_SDL)
  color_t b;
  color_t g;
  color_t r;
  color_t a;
#else
  color_t a;
  color_t b;
  color_t g;
  color_t r;
#endif
} __attribute__((__packed__)) pixel_t; // This is a packed struct

#endif
