#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "image.h"

#ifdef USE_SDL
#define PIXEL_TRANS 0x00FF00FF // The transparent pixel
#else
#define PIXEL_TRANS 0x00FF00FF // The transparent pixel
#endif

#define min(a,b) ((a > b) ? b : a)
#define max(a,b) ((a < b) ? b : a)

typedef struct {
  int x;
  int y;
  int width;
  int height;
} rect_t;

void blit_full(image_t*);
void blit_fast_clip(image_t*, rect_t, int, int);
void blit_fast(image_t*, int, int);
void blit_trans_clip(image_t*, rect_t, int, int);
void blit_trans(image_t*, int, int);
void blit_anim(anim_t*, uint16_t frame, int, int);
void blit_text(anim_t*, const char*, int, int);
void sb_clear(void);
void sb_swap(void);
bool init_graphics(void);
void finish_graphics(void);

#endif
