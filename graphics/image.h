#ifndef READ_IMAGE_H
#define READ_IMAGE_H

#include <stdbool.h>
#include "framebuffer.h"

#define MAX_LINE_LENGTH 1024
#define EXT_IMG "img"
#define EXT_ANI "ani"
#define EXT_PPM "ppm"

typedef enum { I_PPM, I_IMG, I_ANI, I_NONE } image_type_t;

// The static image structure
typedef struct {
  pixel_t* data;     // Pixel data
  uint16_t width;    // Width of image
  uint16_t height;   // Height of image
} image_t;

// The animation structure
typedef struct {
  pixel_t* data;     // Pixel data
  uint16_t width;    // Width of image
  uint16_t height;   // Height of image
  uint16_t f_width;  // Width of a frame
  uint16_t f_height; // Height of a frame
  uint16_t frames;   // Number of frames in animation
  uint16_t delay;    // Frame delay in ms
} anim_t;

// Function prototypes
image_type_t parse_file_ext(const char*);
image_t* read_image(const char*);
anim_t* read_animation(const char*);
void free_image(image_t*);
void free_animation(anim_t*);

bool write_img(image_t*, const char*);
bool write_ani(anim_t*, const char*);

#endif
