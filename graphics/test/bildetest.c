#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>

#include "image.h"
#include "graphics.h"

int main(int argc, char** argv) {
  if (argc < 4) { 
    fprintf(stderr, "Usage: bildetest <background> <charmap> <frameskip>\n");
    exit(EXIT_FAILURE);
  }

  bool graphics_ok = set_charmap(argv[2]) && init_graphics();
  if (!graphics_ok) {
    fprintf(stderr, "Could not init graphics.\n");
    finish_graphics();
    exit(EXIT_FAILURE);
  }

  // Last bakgrunn fra fil
  image_t background; 
  read_image(&background, argv[1]);

  // Les fiskeanimasjon fra fil
  anim_t fish;
  read_anim(&fish, "fish.ani");

  // Les inn frameskip
  int frameskip = strtoul(argv[3], 0, 10);
  frameskip = (frameskip >= 0 && frameskip <= 100) ? frameskip : 2;

  //pos_t middle_x = FB_WIDTH / 2 - image->width / 2;
  //pos_t middle_y = FB_HEIGHT / 2 - image->height / 2;
  double pi_counter_ratio = M_PI / 128.0;

  // Render bakgrunn
  blit_full(&background);

  time_t seconds;
  struct tm* tm;
  struct timeval tv;
  struct timeval tv_last;

  char time_string[128];
  char frame_string[128];

  unsigned long long frame_counter = 0;
  unsigned long long frame_counter_last = 0;
  int pc_last = 0;
  int ps_last = 0;

  int fish_frame;
  
  uint8_t sine_counter = 0;
  for (uint16_t i = -1 ;; i--) {
    frame_counter++;

    if (frame_counter % 10 == 0)
        fish_frame = (fish_frame + 1) % fish.frames;
    
    if (i % (frameskip+1) == 0) {
      sine_counter++;
      double pixel_offset_sin = 150*sin((double)sine_counter * pi_counter_ratio);
      double pixel_offset_cos = 150*cos((double)sine_counter * pi_counter_ratio);

      int ps = i + pixel_offset_sin;
      int pc = i + pixel_offset_cos;

      seconds = time(NULL);
      tm = localtime(&seconds);
      
      gettimeofday(&tv, NULL);
      
      strftime(time_string, sizeof(time_string)/sizeof(time_string[0]), "%A, %d %B - %H:%M:%S", tm);
      snprintf(time_string+strlen(time_string), sizeof(time_string)/sizeof(time_string[0]) - strlen(time_string), ":%lu", tv.tv_usec/1000);
      snprintf(frame_string, sizeof(frame_string)/sizeof(frame_string[0]), "frame: %llu (fps: %.2f)", frame_counter, (double)(frame_counter - frame_counter_last)/(tv.tv_usec - tv_last.tv_usec) * 1000000);

      //fprintf(stderr, "i: %hu sc: %hhu po_sin: %.3f, po_cos: %.3f\n", i, sine_counter, pixel_offset_sin, pixel_offset_cos);
      blit_fast(&background, (rect_t){10, 10, strlen(time_string)*8, 14 * 2}, 10, 10);
      blit_fast(&background, (rect_t){ps_last, pc_last, fish.f_width, fish.f_height}, ps_last, pc_last);

      blit_anim(&fish, fish_frame, ps, pc);
      
      //printf("time_string: '%s' frame_string: '%s'\n", strlen(time_string)*8, time_string, frame_string);
      blit_text(time_string, 10, 10);
      blit_text(frame_string, 10, 10+14);

      tv_last = tv;
      pc_last = pc;
      ps_last = ps;

      sb_swap();
      frame_counter_last = frame_counter;
    }
  }
  
  free_image(&background);
  free_anim(&fish);
  finish_graphics();

  return 0;
}
