#include <stdio.h>
#include <stdlib.h>
#include "framebuffer.h"
#include "image.h"

int main(int argc, char* argv[]) {
  if (argc < 2) {
    fprintf(stderr, "Usage: <read_anim_test> <anim.ani>\n");
    exit(EXIT_FAILURE);
  }

  anim_t anim;
  bool read_ok = read_anim(&anim, argv[1]);
  
  if (!read_ok)
    exit(EXIT_FAILURE);

  // Test output
  printf("[anim] res: %hux%hu, frame_res: %hux%hu, frames: %hu, delay: %hums.\n", anim.width, anim.height, anim.f_width, anim.f_height, anim.frames, anim.delay);
  for (size_t i = 0; i < anim.width*anim.height; i++) {
    printf("[pixel] r: %x, g: %x, b: %x\n", anim.data[i].r, anim.data[i].g, anim.data[i].b);
  }

  free_anim(&anim);

  return 0;
}
