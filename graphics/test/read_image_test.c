#include <stdio.h>
#include <stdlib.h>
#include "framebuffer.h"
#include "image.h"

int main(int argc, char* argv[]) {
  if (argc < 2) {
    fprintf(stderr, "Usage: <read_image_test> <image>\n");
    exit(EXIT_FAILURE);
  }

  image_t image;
  bool read_ok = read_image(&image, argv[1]);
  
  if (!read_ok)
    exit(EXIT_FAILURE);

  // Test output
  printf("[image] width: %d, height: %d\n", image.width, image.height);
  for (size_t i = 0; i < image.width*image.height; i++) {
    printf("[pixel] r: %x, g: %x, b: %x\n", image.data[i].r, image.data[i].g, image.data[i].b);
  }

  free_image(&image);

  return 0;
}
