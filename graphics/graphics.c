#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>

#include "image.h"
#include "graphics.h"

pixel_t* softwarebuffer = NULL;

// Blits part of an image defined by the rectangle argument beginning at (x,y)
// Blits pixel by pixel.
// Transparency supported.
void blit_trans_clip(image_t* img, rect_t rect, int x, int y) {
  if (rect.height <= 0 || rect.width <= 0)
    return;

  for (int j = 0; j < rect.height; j++)
    for (int i = 0; i < rect.width; i++) {
      pixel_t pixel = img->data[((abs(rect.y)+j) % img->height) * img->width + ((abs(rect.x)+i) % img->width)];

      // Only blit if the color is not the predefined transparency color
      if ((pixel.r | pixel.g<<8 | pixel.b<<16) != PIXEL_TRANS) {
        int sb_y = max(min(y + j, FB_HEIGHT), 0);
        int sb_x = max(min(x + i, FB_WIDTH), 0);
        softwarebuffer[sb_y * FB_WIDTH + sb_x] = pixel;
      }
    }
}

// Blits the whole image beginning at (x,y)
inline void blit_trans(image_t* img, int x, int y) {
  blit_trans_clip(img, (rect_t){0, 0, img->width, img->height}, x, y);
}

// Blits the given frame of an animation to the screen
// Blits pixel by pixel.
// Transparency supported.
inline void blit_anim(anim_t* anim, uint16_t frame, int x, int y) {
  frame = frame % anim->frames;
  rect_t rect = { (anim->f_width * frame) % anim->width, ((anim->f_width * frame) / anim->width) * anim->f_height, anim->f_width, anim->f_height };
  blit_trans_clip((image_t*)anim, rect, x, y);
}

// Blits part of an image defined by the rectangle argument beginning at (x,y)
// Blits row by row.
// Transparency not supported.
void blit_fast_clip(image_t* img, rect_t rect, int x, int y) {
  if (rect.height <= 0 || rect.width <= 0)
    return;


  int sb_x = max(min(x, FB_WIDTH - rect.width), 0);
  int blit_height = rect.height + min(y, 0);
  for (int i = 0; i < blit_height; i++) {
    int sb_y = min(max(y, 0)+i, FB_HEIGHT);
    memcpy(softwarebuffer + sb_y * FB_WIDTH + sb_x, img->data + ((abs(rect.y)+i) % img->height) * img->width + (abs(rect.x) % img->width), (rect.width + min(x, 0))*PIXEL_SIZE);
  }
}

// Blits the whole image beginning at (x,y)
inline void blit_fast(image_t* img, int x, int y) {
  blit_fast_clip(img, (rect_t){0, 0, img->width, img->height}, x, y);
}

// Blits an image (presumably) the size of the screen beginning at (0,0).
// Transparency not supported :)
void blit_full(image_t* img) {
  memcpy(softwarebuffer, img->data, min(img->width*img->height*PIXEL_SIZE, FB_SIZE));
}

// Softwarebuffer clear to black.
void sb_clear(void) {
  memset(softwarebuffer, 0, FB_SIZE);
}

// Blits text to the screen. The animation object is presumed to be a map of characters.
void blit_text(anim_t* charmap, const char* string, int x, int y) {
  for (size_t i = 0; i < strlen(string); i++) {
    blit_anim(charmap, string[i], x + i*charmap->f_width, y);
  }
}
