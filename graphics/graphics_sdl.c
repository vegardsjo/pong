#include <SDL2/SDL_surface.h>
#include <SDL2/SDL_video.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include "image.h"
#include "graphics.h"
#include "framebuffer.h"

#include <SDL2/SDL.h>

extern pixel_t* softwarebuffer;

SDL_Window *window = NULL;
SDL_Surface *surface = NULL;

// Softwarebuffer swap; Writes the software buffer to the framebuffer, updating the image on the screen.
void sb_swap(void) {
  SDL_LockSurface(surface);
  memcpy(surface->pixels, softwarebuffer, FB_SIZE);
  SDL_UnlockSurface(surface);
  SDL_UpdateWindowSurface(window);
}

// This function will initialize the graphics subsystem.
bool init_graphics(void) {
  window = SDL_CreateWindow("pong",
                            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                            FB_WIDTH, FB_HEIGHT,
                            SDL_WINDOW_SHOWN);

  surface = SDL_GetWindowSurface(window);
  printf("rmask: %d, gmask: %d, bmask: %d, amask: %d\n", surface->format->Rmask, surface->format->Gmask, surface->format->Bmask, surface->format->Amask);
  softwarebuffer = (pixel_t*)malloc(FB_SIZE);

  if (softwarebuffer == NULL) {
    fprintf(stderr, "Could not allocate software buffer for double buffering: %s\n", strerror(errno));
    return false;
  }
  
  sb_clear();
  sb_swap();
  return true;
}

// This function will deinitialize the graphics subsystem.
void finish_graphics(void) {
  SDL_DestroyWindow(window);
  SDL_Quit();
}
