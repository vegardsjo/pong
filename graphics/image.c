#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include "framebuffer.h"
#include "image.h"

// Utility function to read a 16bit unsigned big endian int
static uint16_t read_u16_be(FILE* fp) {
  uint16_t val;
  fread(&val, sizeof(val), 1, fp);
  val = ntohs(val);
  return val;
}

// Utility function to write a 16bit unsigned big endian int
static void write_u16_be(FILE* fp, uint16_t value) {
  uint16_t val = htons(value);
  fwrite(&val, sizeof(val), 1, fp);
}

// Reads raw pixel data from file
static pixel_t* read_pixel_data(FILE* fp_in, size_t size) {
  pixel_t* data = (pixel_t*)calloc(1, size);

  if (data == NULL) {
    fprintf(stderr, "Could not allocate %lu bytes for image.\n", size);
    return NULL;
  }

  // Reads the pixel data
  fread(data, size, 1, fp_in);
 
  // Fail if any errors
  if (ferror(fp_in) || feof(fp_in)) {
    free(data);
    return NULL;
  }
  
  // Success
  return data;
}

// Reads an image of type ".img" from file
static image_t* read_img(const char* filename) {
  FILE* fp_in = fopen(filename, "rb");

  if (fp_in == NULL) {
    fprintf(stderr, "Cannot open %s for reading: %s\n", filename, strerror(errno));
    return NULL;
  }

  image_t* image = calloc(1, sizeof(image_t));
  image->width = read_u16_be(fp_in);
  image->height = read_u16_be(fp_in);
  
  if (feof(fp_in) || ferror(fp_in)) {
    fprintf(stderr, "Could not read input image file\n");
    free(image);
    return NULL;
  }

  size_t size = sizeof(pixel_t) * image->width * image->height;

  // Read pixel data
  image->data = read_pixel_data(fp_in, size);
  if (image->data == NULL) {
    fprintf(stderr, "Could not read pixel data from input file %s\n", filename);
    fclose(fp_in);
    return NULL;
  }
  
  // Success
  fclose(fp_in);
  return image;
}

// Writes an image of type ".img" to file
bool write_img(image_t* image, const char* filename) {
  FILE* fp_out = fopen(filename, "wb");

  if (fp_out == NULL) {
    fprintf(stderr, "Could not open '%s' for writing: %s\n", filename, strerror(errno));
    return false;
  }

  write_u16_be(fp_out, image->width);
  write_u16_be(fp_out, image->height);

  fwrite(image->data, PIXEL_SIZE * image->width * image->height, 1, fp_out);

  if (ferror(fp_out)) {
    fprintf(stderr, "Could not write to output file %s: %s\n", filename, strerror(errno));
    fclose(fp_out);
    return false;
  }

  // Success
  fclose(fp_out);
  return true;
}

// Reads an animation from file
anim_t* read_animation(const char* filename) {
  FILE* fp_in = fopen(filename, "rb");
  if (fp_in == NULL) {
    fprintf(stderr, "Cannot open %s for reading: %s\n", filename, strerror(errno));
    return NULL;
  }

  anim_t* anim = calloc(1, sizeof(anim_t));
  anim->width = read_u16_be(fp_in);
  anim->height = read_u16_be(fp_in);
  anim->f_width = read_u16_be(fp_in);
  anim->f_height = read_u16_be(fp_in);
  anim->frames = read_u16_be(fp_in);
  anim->delay = read_u16_be(fp_in);
  
  if (feof(fp_in) || ferror(fp_in)) {
    fprintf(stderr, "Could not read input animation file\n");
    free(anim);
    return NULL;
  }

  size_t size = sizeof(pixel_t) * anim->width * anim->height;
  
  // Read pixel data
  anim->data = read_pixel_data(fp_in, size);
  if (anim->data == NULL) {
    fprintf(stderr, "Could not read pixel data from input file %s\n", filename);
    fclose(fp_in);
    return NULL;
  }
  
  // Success
  fclose(fp_in);
  return anim;
}


// This function writes an animation structure to the given filename
bool write_ani(anim_t* anim, const char* filename) {
  FILE* fp_out = fopen(filename, "wb");

  if (fp_out == NULL) {
    fprintf(stderr, "Could not open '%s' for writing: %s\n", filename, strerror(errno));
    return false;
  }

  write_u16_be(fp_out, anim->width);
  write_u16_be(fp_out, anim->height);
  write_u16_be(fp_out, anim->f_width);
  write_u16_be(fp_out, anim->f_height);
  write_u16_be(fp_out, anim->frames);
  write_u16_be(fp_out, anim->delay);

  fwrite(anim->data, sizeof(pixel_t) * anim->width * anim->height, 1, fp_out);

  if (ferror(fp_out)) {
    fprintf(stderr, "Could not write to output file %s: %s\n", filename, strerror(errno));
    fclose(fp_out);
    return false;
  }

  fclose(fp_out);
  return true;
}

// This function will parse a PPM image given by the filename and return an image structure.
static image_t* read_ppm(const char* filename) {
  FILE* fp_in = fopen(filename, "rb");

  if (fp_in == NULL) {
    fprintf(stderr, "Could not open %s for reading: %s\n", filename, strerror(errno));
    return NULL;
  }

  image_t* image = calloc(1, sizeof(image_t));

  // Read buffer
  char buf[MAX_LINE_LENGTH];
  
  // PPM Format string
  char ppm_format[3];

  fgets(buf, MAX_LINE_LENGTH, fp_in);
  memcpy(ppm_format, buf, 2);
  ppm_format[2] = '\0';
  
  // Munch the PPM comment
  fgets(buf, MAX_LINE_LENGTH, fp_in);

  // Read width and height
  fgets(buf, MAX_LINE_LENGTH, fp_in);
  sscanf(buf, "%hu %hu", &image->width, &image->height);
  
  // Munch the maximum color value
  fgets(buf, MAX_LINE_LENGTH, fp_in);
  
  if (ferror(fp_in))
    goto ppm_read_error;

  if (feof(fp_in))
    goto ppm_read_eof;
  
  // Allocate image
  size_t size = sizeof(pixel_t) * image->width * image->height;
  image->data = (pixel_t*)malloc(size);
  if (image->data == NULL) {
    fprintf(stderr, "Could not allocate %lu bytes for image.\n", size);
    fclose(fp_in);
    return NULL;
  }

  // RAW PPM
  if (strcmp(ppm_format, "P6") == 0) {
    for (size_t i = 0; i < image->width * image->height && !feof(fp_in) && !ferror(fp_in); i++) {
      fscanf(fp_in, "%c%c%c", &image->data[i].r, &image->data[i].g, &image->data[i].b);
    }
  }
  // ASCII PPM
  else if (strcmp(ppm_format, "P3") == 0) {
    for (size_t i = 0; i < image->width * image->height && !feof(fp_in) && !ferror(fp_in); i++) {
      fgets(buf, MAX_LINE_LENGTH, fp_in);
      sscanf(buf, "%hhu", &image->data[i].r);
      fgets(buf, MAX_LINE_LENGTH, fp_in);
      sscanf(buf, "%hhu", &image->data[i].g);
      fgets(buf, MAX_LINE_LENGTH, fp_in);
      sscanf(buf, "%hhu", &image->data[i].b);
    }
  }
  else {
    fprintf(stderr, "Image is not an ASCII or binary PPM image (format: '%s').\n", ppm_format);
    free(image->data);
    fclose(fp_in);
    return NULL;
  }
  
  if (ferror(fp_in))
    goto ppm_read_error;

  if (feof(fp_in))
    goto ppm_read_eof;
  
  // Success
  fclose(fp_in);
  return image;

  
  ppm_read_eof:
    fprintf(stderr, "End of file encountered while reading from input file '%s'\n", filename);
    free(image->data);
    fclose(fp_in);
    return NULL;

  ppm_read_error:
    fprintf(stderr, "Could not read from input file '%s'\n", filename);
    free(image->data);
    fclose(fp_in);
    return NULL;
}

// Gets image/animation type from file extension.
image_type_t parse_file_ext(const char* filename) {
  char* token = strrchr(filename, '.');
  if (token == NULL || strlen(token) < 3)
    return I_NONE;
  
  // Seek to extension
  token++;
  
  if (strncmp(token, EXT_IMG, 3) == 0)
    return I_IMG;
  else
  if (strncmp(token, EXT_PPM, 3) == 0)
    return I_PPM;
  else
  if (strncmp(token, EXT_ANI, 3) == 0)
    return I_ANI;
  else
    return I_NONE;
}

// Reads a .img or .ppm image from file
image_t* read_image(const char* filename) {
  image_t* image = NULL;
  
  image_type_t type = parse_file_ext(filename);
  switch (type) {
    case I_IMG:
      image = read_img(filename);
      break;
    case I_PPM:
      image = read_ppm(filename);
      break;
    default:
      image = NULL;
  }

  return image;
}

// Deallocates an image
inline void free_image(image_t* image) {
  if (image == NULL)
    return;

  if (image->data == NULL) {
    free(image);  
    return;
  }

  free(image->data);
  free(image);
}

// Deallocates an animation
inline void free_animation(anim_t* anim) {
  if (anim == NULL)
    return;

  if(anim->data == NULL) {
    free(anim);
    return;
  }

  free(anim->data);
  free(anim);
}
