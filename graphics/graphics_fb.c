#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include "image.h"
#include "graphics.h"
#include "framebuffer.h"

extern pixel_t* softwarebuffer;
static pixel_t* framebuffer = NULL;

// Softwarebuffer swap; Writes the software buffer to the framebuffer, updating the image on the screen.
void sb_swap(void) {
  memcpy(framebuffer, softwarebuffer, FB_SIZE);
  msync(framebuffer, FB_SIZE, MS_SYNC);
}

// This function will initialize the graphics subsystem.
bool init_graphics(void) {
  // Open the framebuffer device for writing, and allocate a software buffer to allow for double buffering.
  int fd = open(FB_DEV, O_RDWR);
  softwarebuffer = (pixel_t*)malloc(FB_SIZE);

  if (softwarebuffer == NULL) {
    fprintf(stderr, "Could not allocate software buffer for double buffering: %s\n", strerror(errno));
    return false;
  }
  
  // Memory map the framebuffer device
  framebuffer = (pixel_t*)mmap(NULL, FB_SIZE, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);

  if (framebuffer == MAP_FAILED) {
    fprintf(stderr, "Could not map the framebuffer to memory: %s\n", strerror(errno));
    return false;
  }
  
  // Clear/Blacken the software and framebuffer
  sb_clear();
  sb_swap();
  return true;
}

// This function will deinitialize the graphics subsystem.
void finish_graphics(void) {
  free(softwarebuffer);
  munmap(framebuffer, FB_SIZE);
}
