URL="http://folk.ntnu.no/~sjonfjel/"
g() { wget "$URL/$@"; }

mkdir wallpapers
cd wallpapers
g Groundhog_amid_the_Golden_Fields.img
g 10,000_Galaxies.img
g Siberian_Tiger.img
g The_Horsehead_Orion_Nebula.img
g underwater_2.img
g Mir_Space_Station_Observing.img
g Evil_cat.img
g The_Andromeda_Spyral_Galaxy.img
cd ..

mkdir resources
cd resources
g ball.img
g logo.img
g paddle_left.ani
g paddle_right.ani
g player_one_wins.img
g player_two_wins.img
g charmap.ani
g textbox.img
g coin.wav
g die.wav
g wall.wav
g hit.wav
g win.wav
cd ..

g pong.elf
g unified_driver.ko

