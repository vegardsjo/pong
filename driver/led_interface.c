#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "utils/endian.h"
#include "led_interface.h"

int open_leds(void) {
  int fd = open(LED_DEV, O_WRONLY);

  if (fd < 0) {
    fprintf(stderr, "Could not open leds device. LED output disabled.\n");
    return -1;
  }

  return fd;
}

void write_leds(int fd, led_setting_t setting) { 
  write(fd, &setting, sizeof(setting));
}
