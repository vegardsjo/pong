#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "utils/endian.h"
#include "button_interface.h"

int open_buttons(void) {
  int fd = open(BUTTON_DEV, O_RDONLY);

  if (fd < 0) {
    fprintf(stderr, "Could not open buttons device. Input disabled.\n");
    return -1;
  }

  return fd;
}

button_setting_t read_buttons(int fd) { 
  button_setting_t setting;
  read(fd, &setting, sizeof(setting));
  return setting;
}
