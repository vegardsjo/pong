#ifndef LED_INTERFACE_H
#define LED_INTERFACE_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#define LED_DEV "/dev/leds"

typedef union {
  struct {
    bool led7:1;
    bool led6:1;
    bool led5:1;
    bool led4:1;
    bool led3:1;
    bool led2:1;
    bool led1:1;
    bool led0:1;
  };
  uint8_t LED;
} led_setting_t;

int open_leds(void);
void write_leds(int, led_setting_t);

#endif
