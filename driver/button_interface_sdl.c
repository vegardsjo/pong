#include <SDL2/SDL_events.h>
#include <SDL2/SDL_keycode.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "utils/endian.h"
#include "button_interface.h"
#include <SDL2/SDL.h>

int open_buttons(void) {
  return 0;
}

static button_setting_t setting;
button_setting_t read_buttons(int fd) {
  (void)fd;

  SDL_Event event;
  while (SDL_PollEvent(&event)) {
    switch (event.key.keysym.sym) {
    case SDLK_DOWN: // Paddle right down
      setting.sw0 = event.key.type != SDL_KEYUP;
      break;
    case SDLK_y: // AI right
      setting.sw1 = event.key.type != SDL_KEYUP;
      break;
    case SDLK_UP: // Paddle right up
      setting.sw2 = event.key.type != SDL_KEYUP;
      break;
    case SDLK_r: // Reset
      setting.sw3 = event.key.type != SDL_KEYUP;
      break;
    case SDLK_ESCAPE: // Quit
      setting.sw4 = event.key.type != SDL_KEYUP;
      break;
    case SDLK_s: // Paddle left down
      setting.sw5 = event.key.type != SDL_KEYUP;
      break;
    case SDLK_t: // AI right
      setting.sw6 = event.key.type != SDL_KEYUP;
      break;
    case SDLK_w: // Paddle left up
      setting.sw7 = event.key.type != SDL_KEYUP;
      break;
    default:
      break;
    }
  }

  return setting;
}
