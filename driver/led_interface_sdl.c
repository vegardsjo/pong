#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "utils/endian.h"
#include "led_interface.h"

int open_leds(void) {
  return 0;
}

void write_leds(int fd, led_setting_t setting) {
  (void)fd;
  (void)setting;
}
