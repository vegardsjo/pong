/****************************************/
/*                                      */
/* TDT4258 Mikrokontroller systemdesign */
/* Øving 3                              */
/*                                      */
/* sjonfjel                             */
/* tirilane                             */
/*                                      */
/* unified_driver.h                     */
/*                                      */
/****************************************/

#ifndef UNIFIED_DRIVER_H
#define UNIFIED_DRIVER_H

typedef struct {
  char b7:1;
  char b6:1;
  char b5:1;
  char b4:1;
  char b3:1;
  char b2:1;
  char b1:1;
  char b0:1;
} byte;

static int driver_open (struct inode*, struct file*);
static int driver_release (struct inode*, struct file*);
static int led_device_read (struct file*, char __user*, size_t, loff_t*);
static int led_device_write (struct file*, const char __user*, size_t, loff_t*);
static int button_device_read (struct file*, char __user*, size_t, loff_t*);
static int button_device_write (struct file*, const char __user*, size_t, loff_t*);

#endif

