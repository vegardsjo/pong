#ifndef BUTTON_INTERFACE_H
#define BUTTON_INTERFACE_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#define BUTTON_DEV "/dev/buttons"

typedef union {
  struct {
    bool sw7:1;
    bool sw6:1;
    bool sw5:1;
    bool sw4:1;
    bool sw3:1;
    bool sw2:1;
    bool sw1:1;
    bool sw0:1;
  };
  uint8_t SW;
} button_setting_t;

int open_buttons(void);
button_setting_t read_buttons(int);

#endif
