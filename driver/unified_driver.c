/****************************************/
/*                                      */
/* TDT4258 Mikrokontroller systemdesign */
/* Øving 3                              */
/*                                      */
/* sjonfjel                             */
/* tirilane                             */
/*                                      */
/* led_driver.c                         */
/*                                      */
/****************************************/

#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/ioport.h>
#include <asm/io.h>
#include <asm/uaccess.h>

#include "ap7000.h"
#include "unified_driver.h"

static char* name = "unified_driver";
static dev_t dev_buttons;
static dev_t dev_leds;
static int num_devices = 1;
static int minor = 0;
static struct cdev *cdev_buttons = NULL;
static struct cdev *cdev_leds = NULL;
static volatile avr32_pio_t *pio = NULL;

static const int buttons_bitmask = 0xFF;
static const int leds_bitmask = 0x4001E700;
  

/* fops-struct */
static struct file_operations led_device_fops = {
  .owner = THIS_MODULE,
  .read = led_device_read,
  .write = led_device_write,
  .open = driver_open,
  .release = driver_release,
};

static struct file_operations button_device_fops = {
  .owner = THIS_MODULE,
  .read = button_device_read,
  .write = button_device_write,
  .open = driver_open,
  .release = driver_release,
};

/* init-funksjon (kalles når modul lastes) */
static int __init driver_init (void) {
  struct resource *res;
  int result;

  /** Allokerer device-nummer ***/
  result = alloc_chrdev_region (&dev_leds, minor, num_devices, "led device");
  if (result) {
    printk (KERN_EMERG "%s: Could not allocate character device for LED.\n", name);
    return -EINVAL; // Invalid argument.
  }
  
  result = alloc_chrdev_region (&dev_buttons, minor, num_devices, "buttons device");
  if (result) {
    printk (KERN_EMERG "%s: Could not allocate character device for Buttons.\n", name);
    return -EINVAL; // Invalid argument.
  }

  cdev_leds = cdev_alloc ();
  cdev_init (cdev_leds, &led_device_fops);

  cdev_buttons = cdev_alloc ();
  cdev_init (cdev_buttons, &button_device_fops);

  res = request_region (AVR32_PIOB_ADDRESS, AVR32_PIOB_PIOB_LINES, "PIO B leds/buttons");
  if (res == NULL) {
    printk (KERN_EMERG "%s: Could not request memory region for PIO B.\n", name);
    return -ENOMEM; // Out of memory.
  }

  pio = (avr32_pio_t*)ioremap (AVR32_PIOB_ADDRESS, AVR32_PIOB_PIOB_LINES);
  if (pio == NULL) {
    printk (KERN_EMERG "%s: Could not ioremap memory region for PIO B.\n", name);
    return -ENOMEM; // Out of memory.
  }

  /*** Registrerer device i systemet ***/
  result = cdev_add (cdev_leds, dev_leds, 1);
  if (result) {
    printk (KERN_EMERG "%s: Could not add character device for LED.\n", name);
    return -EINVAL; // Invalid argument.
  }

  result = cdev_add (cdev_buttons, dev_buttons, 1);
  if (result) {
    printk (KERN_EMERG "%s: Could not add character device for Buttons.\n", name);
    return -EINVAL; // Invalid argument.
  }

  /*** Sett opp IO-pinner ***/
  pio->per = buttons_bitmask | leds_bitmask;
  
  // Knapper pinner 0-7
  pio->odr = buttons_bitmask;  // Deaktiver output for alle knapper
  pio->puer = buttons_bitmask; // Aktiver pull-up for alle knapper

  // Lysdioder pinner 8-10, 13-16, 30
  pio->pudr = leds_bitmask; // Deaktiver pull-up for alle lysdioder
  pio->oer = leds_bitmask;  // Aktiver output for alle lysdioder
  pio->codr = leds_bitmask; // Skru av alle pinner til å begynne med

  printk (KERN_INFO "%s: LED/Button unified driver loaded.\n", name);

  return 0; // Success.
}

/* exit-funksjon (kalles når modul fjernes fra systemet) */
static void __exit driver_exit (void) {
  release_region(AVR32_PIOB_ADDRESS, AVR32_PIOB_PIOB_LINES);
  cdev_del (cdev_leds);
  unregister_chrdev_region (dev_leds, 1);
  cdev_del (cdev_buttons);
  unregister_chrdev_region (dev_buttons, 1);

  printk (KERN_INFO "%s: LED/Button unified driver unloaded.\n", name);
}

/* fops-funksjoner */
static int driver_open (struct inode *inode, struct file *fp) {
  return 0;
}

static int driver_release (struct inode *inode, struct file *fp) {
  return 0;
}

static ssize_t button_device_read (struct file *fp, char __user *buf, size_t count, loff_t *offp) {
  u8 buttons = ~pio->pdsr;
  
  if (count >= 1) {
    copy_to_user(buf, &buttons, sizeof(buttons));
    return sizeof(buttons);
  }

  return 0;
}

static ssize_t button_device_write (struct file *fp, const char __user *buf, size_t count, loff_t *offp) {
  return -ENOSYS; // Function not implemented
}

static ssize_t led_device_read (struct file *fp, char __user *buf, size_t count, loff_t *offp) {
  return -ENOSYS; // Function not implemented
}

static ssize_t led_device_write (struct file *fp, const char __user *buf, size_t count, loff_t *offp) {
  byte leds;

  if (count >= 1) {
    copy_from_user(&leds, buf, sizeof(leds));
   
    //pio->codr = leds_bitmask; 
    
    pio->SODR.p8 = leds.b0;
    pio->SODR.p9 = leds.b1;
    pio->SODR.p10 = leds.b2;
    pio->SODR.p13 = leds.b3;
    pio->SODR.p14 = leds.b4;
    pio->SODR.p15 = leds.b5;
    pio->SODR.p16 = leds.b6;
    pio->SODR.p30 = leds.b7;
    
    pio->CODR.p8 = !leds.b0;
    pio->CODR.p9 = !leds.b1;
    pio->CODR.p10 = !leds.b2;
    pio->CODR.p13 = !leds.b3;
    pio->CODR.p14 = !leds.b4;
    pio->CODR.p15 = !leds.b5;
    pio->CODR.p16 = !leds.b6;
    pio->CODR.p30 = !leds.b7;

    return sizeof(leds);
  }

  return 0;
}

module_init (driver_init);
module_exit (driver_exit);

MODULE_LICENSE ("GPL");
MODULE_DESCRIPTION ("Driver for STK1000/AVR32 leds og knapper");
MODULE_VERSION ("1.0");
MODULE_AUTHOR ("sjonfjel, tirilane");
