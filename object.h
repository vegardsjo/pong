#ifndef OBJECT_H
#define OBJECT_H

#include <stdint.h>
#include <stdbool.h>
#include "graphics/framebuffer.h"
#include "graphics/image.h"
#include "graphics/graphics.h"

// Vector type
typedef struct {
  double deg;
  double val;
} vec_t;

// Tuple type for two-valued return values
typedef struct {
  int32_t x;
  int32_t y;
} tuple_t;

// Sprite type enum
typedef enum { S_IMAGE, S_ANIMATION } sprite_type_t;

// The object type.
// The sprite (image) for an object can either be a static one or an animation
typedef struct {
  int x;                          // x position
  int y;                          // y position
  int width;                      // width of object
  int height;                     // height of object 
  vec_t speed;                    // speed vector
  rect_t box;                     // collision box
  uint32_t last_draw;             // time since last drawing

  sprite_type_t sprite_type;      // sprite type: image or animation
  union {
    image_t* image;               // if image, the image
    anim_t* anim;                 // if animation, the animation
  };
  
  uint32_t frame;                 // current frame of animation
  uint32_t frame_time;            // ms since last frame change
} object_t;

// Function prototypes
object_t* object_new(const char*, int, int);
void object_free(volatile object_t*);
void object_move(volatile object_t*, int, int);
void object_stop(volatile object_t*);
void object_update_pos(volatile object_t*);
void object_draw(volatile object_t*);
void object_draw_below(volatile image_t*, volatile object_t*);
tuple_t object_calc_next_disp(volatile object_t*);
tuple_t object_calc_dist(volatile object_t*, volatile object_t*);
bool object_collides(volatile object_t*, volatile object_t*, int, int);

#endif
