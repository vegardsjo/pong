#ifndef ENDIAN_H
#define ENDIAN_H

#include <stdint.h>

uint32_t swap32(uint32_t);
uint32_t le32toho(uint32_t);
uint32_t be32toho(uint32_t);

uint16_t swap16(uint16_t);
uint16_t le16toho(uint16_t);
uint16_t be16toho(uint16_t);

#endif
