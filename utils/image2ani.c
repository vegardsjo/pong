#include <stdio.h>
#include <stdlib.h>
#include "graphics/framebuffer.h"
#include "graphics/image.h"

int main(int argc, char* argv[]) {
  if (argc < 6) {
    fprintf(stderr, "Usage: <image2ani> <image.ppm/img> <animation.ani> <frame width> <frame height> <frame delay (ms)> [number of frames]\n");
    exit(EXIT_FAILURE);
  }

  image_t* image = read_image(argv[1]);
  if (image == NULL)
    exit(EXIT_FAILURE);
  
  anim_t* anim = calloc(1, sizeof(anim_t));
  anim->width = image->width;
  anim->height = image->height;
  anim->data = image->data;
  anim->f_width = (uint16_t)strtoul(argv[3], NULL, 10);
  anim->f_height = (uint16_t)strtoul(argv[4], NULL, 10);
  anim->delay = (uint16_t)strtoul(argv[5], NULL, 10);

  if (argc > 6)
    anim->frames = (uint16_t)strtoul(argv[6], NULL, 10);
  else
    anim->frames = (anim->width / anim->f_width) * (anim->height / anim->f_height);

  bool write_ok = write_ani(anim, argv[2]);
  if (!write_ok) {
    free_animation(anim);
    exit(EXIT_FAILURE);
  }
  
  free_image(image);
  anim->data = NULL;
  free_animation(anim);
  return EXIT_SUCCESS;
}

