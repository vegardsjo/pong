#include <stdio.h>
#include <stdlib.h>
#include "graphics/framebuffer.h"
#include "graphics/image.h"

int main(int argc, char* argv[]) {
  if (argc < 3) {
    fprintf(stderr, "Usage: <image2raw> <image.ppm/img> <image.img>\n");
    exit(EXIT_FAILURE);
  }

  image_t* image = read_image(argv[1]);
  
  if (image == NULL)
    exit(EXIT_FAILURE);

  bool write_ok = write_img(image, argv[2]);
  if (!write_ok) {
    free_image(image);
    exit(EXIT_FAILURE);
  }

  free_image(image);
  return EXIT_SUCCESS;
}

