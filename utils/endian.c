#include <stdint.h>

typedef struct {
  uint8_t b0;
  uint8_t b1;
  uint8_t b2;
  uint8_t b3;
} uint32_st;

typedef struct {
  uint8_t b0;
  uint8_t b1;
} uint16_st;

uint32_t swap32(uint32_t val) {
  return *(uint32_t*)&(uint32_st){ .b0 = *(((uint8_t*)&val)+3), .b1 = *(((uint8_t*)&val)+2), .b2 = *(((uint8_t*)&val)+1), .b3 = *(uint8_t*)&val };
}

uint32_t swap16(uint16_t val) {
  return *(uint16_t*)&(uint16_st){ .b0 = *(((uint8_t*)&val)+1), .b1 = *((uint8_t*)&val) };
}

uint32_t le32toho(uint32_t val) {
  return val;
}

uint32_t be32toho(uint32_t val) {
  return swap32(val);
}

uint16_t le16toho(uint16_t val) {
  return val;
}

uint16_t be16toho(uint16_t val) {
  return swap16(val);
}
