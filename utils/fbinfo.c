#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include "fb.h"

const char* device = "/dev/fb0";

int main(void) {
  FILE* fp_fb = fopen(device, "rb");
  if (fp_fb == NULL) {
    fprintf(stderr, "Could not open '%s' for reading: %s\n", device, strerror(errno));
    exit(1);
  }
    
  struct fb_fix_screeninfo fs;
  struct fb_var_screeninfo fv;
  
  ioctl(fileno(fp_fb), FBIOGET_FSCREENINFO, &fs);
  ioctl(fileno(fp_fb), FBIOGET_VSCREENINFO, &fv);

  printf("id: %s (mem_size: %d) (mem_start: 0x%lx)\n", fs.id, fs.smem_len, fs.smem_start);
  printf("xres: %d, yres: %d, bpp: %d\n", fv.xres, fv.yres, fv.bits_per_pixel);
  printf("[red] offset: %d, length: %d, %s endian\n", fv.red.offset, fv.red.length, fv.red.msb_right == 0 ? "little" : "big");
  printf("[blue] offset: %d, length: %d, %s endian\n", fv.blue.offset, fv.blue.length, fv.blue.msb_right == 0 ? "little" : "big");
  printf("[green] offset: %d, length: %d, %s endian\n", fv.green.offset, fv.green.length, fv.green.msb_right == 0 ? "little" : "big");

  return 0;
}

