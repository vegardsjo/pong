#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <sys/time.h>
#include <math.h>
#include "object.h"

// Object contstructor. Creates a new object with sprite from given filename and places it at (x,y).
object_t* object_new(const char* imagefile, int x, int y) {
  object_t* obj = calloc(1, sizeof(object_t));
  
  image_type_t type = parse_file_ext(imagefile);
  switch (type) {
    // Static image
    case I_IMG:
    case I_PPM:
      obj->sprite_type = S_IMAGE;
      obj->image = read_image(imagefile);
      if(obj->image == NULL) goto read_error;

      obj->width = obj->image->width;
      obj->height = obj->image->height;
      break;
    // Animation
    case I_ANI:
      obj->sprite_type = S_ANIMATION;
      obj->anim = read_animation(imagefile);
      if(obj->anim == NULL) goto read_error;

      obj->width = obj->anim->f_width;
      obj->height = obj->anim->f_height;
      break;
    default:
      fprintf(stderr, "Image filename %s not recognized.\n", imagefile);
      free(obj);
      return NULL;
  }
  
  obj->x = x;
  obj->y = y;
  obj->box = (rect_t){x-obj->width/2, y - obj->height/2, obj->width, obj->height};

  return obj;
  
  read_error:
    fprintf(stderr, "Could not read image file %s\n", imagefile);
    free(obj);
    return NULL;
}

// Deallocates the object
void object_free(volatile object_t* obj) {
  if (obj == NULL)
    return;

  if (obj->sprite_type == S_IMAGE)
    free(obj->image);
  else
    free(obj->anim);

  free(obj);
}

// Moves object to the given coordinate
void object_move(volatile object_t* obj, int x, int y) {
  
  // Check for boundary conditions
  if      (x - obj->width/2 < 0) x = obj->width/2;
  else if (x + obj->width/2 > FB_WIDTH) x = FB_WIDTH - obj->width/2;
  if      (y - obj->height/2 < 0) y = obj->height/2;
  else if (y + obj->height/2 > FB_HEIGHT) y = FB_HEIGHT - obj->height/2;

  obj->x = x;
  obj->y = y;
  obj->box.x = x - obj->width/2;
  obj->box.y = y - obj->height/2;
}

void object_stop(volatile object_t* obj) {
  obj->speed.val = 0;
}

// Updates the object position according to the speed vector.
void object_update_pos(volatile object_t* obj) {
  tuple_t disp = object_calc_next_disp(obj);
  object_move(obj, obj->x + disp.x, obj->y + disp.y);
}

// (Internal function) Draws and object with a static image to the screen.
static void _object_draw_image(volatile object_t* obj) {
  blit_trans(obj->image, obj->x - obj->image->width / 2, obj->y - obj->image->width / 2);
}

// (Internal function) Draws an object with an animation to the screen.
static void _object_draw_anim(volatile object_t* obj) {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  time_t now = tv.tv_usec / 1000;
  
  // Update frame time and advance frame if it's over the frame time
  time_t time_delta = (obj->last_draw > now) ? (1000 + now - obj->last_draw): now - obj->last_draw;
  obj->frame_time += time_delta;
  //printf("[%p] frame: %lu\n", obj, obj->frame);

  if (obj->frame_time >= obj->anim->delay) {
    obj->frame = (obj->frame + 1) % obj->anim->frames;
    obj->frame_time = 0;
  }

  blit_anim(obj->anim, obj->frame, obj->x - obj->anim->f_width / 2, obj->y - obj->anim->f_height / 2);
  obj->last_draw = now;
}

// Draws the object to the screen
void object_draw(volatile object_t* obj) {
  if (obj->sprite_type == S_IMAGE)
    _object_draw_image(obj);
  else
    _object_draw_anim(obj);
}

void object_draw_below(volatile image_t* background, volatile object_t* obj) {
  rect_t obj_rect = (rect_t){obj->box.x-5, obj->box.y-5, obj->width+5, obj->height+5};
  //blit_fast_clip(background, obj_rect, obj->x - width/2, obj->y - height/2);
  blit_fast_clip(background, obj_rect, obj_rect.x, obj_rect.y);
  //printf("{%d, %d, %d, %d}, %d,%d\n", obj_rect.x, obj_rect.y, obj_rect.width, obj_rect.width, obj->x - width/2, obj->y - height/2);
}

// Gets the next displacement (from the vector) in pixels
tuple_t object_calc_next_disp(volatile object_t* obj) {
  return (tuple_t){(int32_t)(cos(obj->speed.deg) * obj->speed.val), -(int32_t)(sin(obj->speed.deg) * obj->speed.val)}; 
}

// Gets the distance between object a and b
tuple_t object_calc_dist(volatile object_t* a, volatile object_t* b) {
  return (tuple_t){(int32_t)b->x - (int32_t)a->x, (int32_t)b->y - (int32_t)a->y};
}

// Returns true if a collides with b when moved according to x_diff and y_diff
bool object_collides(volatile object_t* a, volatile object_t* b, int x_diff, int y_diff) {
  int a_left_edge, a_right_edge, a_top_edge, a_bottom_edge;
  int b_left_edge, b_right_edge, b_top_edge, b_bottom_edge;
  
  a_left_edge = a->box.x + x_diff;
  a_right_edge = a->box.x + x_diff + a->box.width;
  a_top_edge = a->box.y + y_diff;
  a_bottom_edge = a->box.y + y_diff + a->box.height;

  b_left_edge = b->box.x;
  b_right_edge = b->box.x + b->box.width;
  b_top_edge = b->box.y;
  b_bottom_edge = b->box.y + b->box.height;

  if (a_right_edge < b_left_edge)
    return false;
  else
  if (a_left_edge > b_right_edge)
    return false;
  else
  if (a_top_edge > b_bottom_edge)
    return false;
  else
  if (a_bottom_edge < b_top_edge)
    return false;
 
  // We have a collision
  return true;
}
