#ifndef PONG_H
#define PONG_H
#include <pthread.h>
#include <dirent.h>
#include "sound/sound.h"

#define PADDLE_MOVE_DEFAULT 3
#define BALL_SPEED_DEFAULT 3
#define BALL_SPEED_MAX 7
#define BALL_SPEED_INC_FACTOR 1.05
#define FRAMES_PER_SECOND 60
#define WIN_POINTS 5

#ifdef USE_SDL
#define WALLPAPERS "wallpapers_x86/"
#define RESOURCES "resources_x86/"
#else
#define RESOURCES "resources/"
#define WALLPAPERS "wallpapers/"
#endif

#define LINE_MAX (320 / 8)

typedef enum { DIR_DOWN=-1, DIR_UP=1 } direction_t;
typedef enum { SOUND_HIT=0, SOUND_WALL, SOUND_DIE, SOUND_COIN, SOUND_WIN, NUM_SOUNDS } soundnum_t;
typedef enum { PADDLE_LEFT, PADDLE_RIGHT } paddlenum_t;

typedef struct {
  // State variables
  volatile bool program_running;
  volatile bool game_running;
  volatile int player_one_score;
  volatile int player_two_score;

  // Images and animations
  volatile image_t* background;
  volatile image_t* textbox;
  volatile anim_t* charmap;

  // Wallpaper list
  volatile size_t num_wallpapers;
  volatile char **wallpapers;
 
  // Objects
  volatile object_t* ball;
  volatile object_t* paddle_left;
  volatile object_t* paddle_right;

  // Ai flags
  volatile bool ai_left;
  volatile bool ai_right;
} Pong_t;

typedef struct {
  volatile bool new_sound;
  volatile soundnum_t current_sound;
  const sound_t** sounds;
} Sound_t;

typedef struct {
  bool threads_started;
  pthread_t game_thread;
  pthread_t device_thread;
  pthread_t sound_thread;
  
  pthread_mutex_t exit_mutex;
} Threads_t;

bool init_threads(void);
bool init_pong(void);
bool init_sound(void);

void finish_pong(void);
void finish_sound(void);
void finish_app(void);

bool has_value_about(double, double, double);
void fps_calc(void);
void fps_limit(int);

void draw_text_vararg(int, int, const char*, va_list);
void draw_text(int, int, const char*, ...);
void announce_string(const char*, ...);
void announce_image(image_t*);
void announce_image_file(const char*);

void set_sound(soundnum_t);

void reposition_ball(void);
void set_random_background(void);
void reset_game(void);

char **get_wallpaper_list(size_t*);
void free_wallpaper_list(char**, size_t);
#endif
